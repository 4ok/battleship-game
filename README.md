# Battleship Game

**Nama kelompok 4 :**
- 4OK

**Anggota kelompok :**
- Carissa Syieva Qalbiena
- Fathur Rahman Prawira
- Putri Salsabila

**Penggunaan aplikasi :**
- Pada tampilan awal akan ditampilkan message "Battleship Game". Semua LED akan nyala secara bergiliran, menandakan bahwa game akan mulai. LED 0-7 nyala, menandakan permulaian permainan, dengan LED 0, 2, 4 dan 6 berwarna biru dan LED berwarna 1, 3, 5, 7 berwarna merah. 
- Setelah tampilan awal selesai dilewati, Player 1 akan diminta untuk input semua ship yang dimiliki. LED biru yaitu LED ke 0, 2, 4 dan 6 nyala, menandakan bahwa sekarang giliran pemain pertama. Masing-masing player memiliki 3 ship yang perlu di input. Ketika Player 1 selesai input semua ship, pergiliran beralih ke Player 2. LED merah yaitu LED ke 1, 3, 5 dan 7 nyala, menandakan bahwa sekarang giliran pemain kedua.
- Kemudian Player 1 akan diminta untuk memilih petak yang akan ditembak. LED biru yaitu LED ke 0, 2, 4 dan 6 nyala, menandakan bahwa sekarang giliran pemain pertama. Cara menembak petak yaitu dengan klik button Shot. Apabila player ingin memilih petak lain, player bisa melakukannya dengan klik button Next, untuk memilih petak di samping kanan current petak. Masing-masing player memiliki kesempatan untuk menembak sebanyak 3x sebelum perpindahan giliran menembak ke player selanjutnya.
- Setelah Player 1 selesai menembak kapal di petak pilihannya, pergiliran akan pindah ke Player 2. Player 2 kemudian diminta untuk memilih petak yang akan ditembak. LED merah yaitu LED ke 1, 3, 5 dan 7 nyala, menandakan bahwa sekarang giliran pemain kedua.
- Player 1 & 2 saling bergiliran dalam menembak ship. Jika player berhasil menembak kapal lawan (hit) maka player tersebut akan mendapatkan poin.
- Game berakhir ketika seluruh kapal salah satu pemain berhasil ditenggelamkan (ditembak).
- Setelah game berakhir, akan muncul tampilan akhir yaitu message berisi "PX, Game Over", dengan X adalah pemain yang menang. Pemenang adalah player yang berhasil menenggelamkan semua kapal lawannya terlebih dahulu. Semua LED nyala kedap-kedip, menandakan bahwa game telah berakhir.
