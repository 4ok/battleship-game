;=======================================
; Processor		: ATmega8515
; Compiler		: AVRASM
;=======================================

.include "m8515def.inc"

; Counter
.def turn_player = r3 ; 0 = P1, 1 = P2
.def stage_game = r4 ; 0 = input ship, 1 = shot ship

; Player's Ship
.def p1_ship = r5
.def p2_ship = r6

; Temporary Register
.def temp = r16
.def temp2 = r17
.def lamp = r26

.def check_int = r18
.def EW = r23 ; for PORTA
.def PB = r24 ; for PORTB
.def A  = r25

; Rows for Player 1
.equ P1_ROW0 = 0x80
.equ P1_ROW1 = 0xC0
.equ P1_ROW2 = 0x94
.equ P1_ROW3 = 0xD4

; Rows for Player 2
.equ P2_ROW0 = 0x90
.equ P2_ROW1 = 0xD0
.equ P2_ROW2 = 0xA4
.equ P2_ROW3 = 0xE4

; Data Pointer
.equ P1_DATA = 0x0060 ; SRAM 60-6F contains data for P1
.equ P2_DATA = 0x0070 ; SRAM 70-7F contains data for P2

; Char Block
.equ FF = 0xFF

;=======================================

.org $00
rjmp MAIN
.org $01
rjmp NEXT_BTN
.org $02
rjmp SHOT_BTN

;=======================================

MAIN:
	ldi check_int, $00

INIT_STACK:
	ldi temp, low(RAMEND)
	ldi temp, high(RAMEND)
	out SPH, temp

INIT_INTERRUPT:
	ldi temp,0b00001010	; Untuk 2 button
	out MCUCR,temp
	ldi temp,0b11000000
	out GICR,temp
	sei

INIT_LED:
	ldi lamp, 0xFF
	out PORTC, lamp

rcall INIT_LCD_MAIN

EXIT:
	rjmp EXIT

; NEXT button
NEXT_BTN:
	rcall INIT_CURSOR_LCD

	cpi temp, 0x83
	breq LIMIT_R1P1
	cpi temp, 0xC3
	breq LIMIT_R2P1
	cpi temp, 0x97
	breq LIMIT_R3P1
	cpi temp, 0xD7
	breq LIMIT_R4P1

	cpi temp, 0x93
	breq LIMIT_R1P2
	cpi temp, 0xD3
	breq LIMIT_R2P2
	cpi temp, 0xA7
	breq LIMIT_R3P2
	cpi temp, 0xE7
	breq LIMIT_R4P2
	
	ldi temp2, 1
	add temp, temp2
	rjmp NEXT_CURSOR
	//reti

LIMIT_R1P1:
	; if temp = x83, temp = xC0
	ldi temp, 0xC0
	rjmp NEXT_CURSOR
LIMIT_R2P1:
	; if temp = xC3, temp = x94
	ldi temp, 0x94
	rjmp NEXT_CURSOR
LIMIT_R3P1:
	; if temp = x97, temp = xD4
	ldi temp, 0xD4
	rjmp NEXT_CURSOR
LIMIT_R4P1:
	; if temp = xD7, temp = x80
	ldi temp, 0x80
	rjmp NEXT_CURSOR

LIMIT_R1P2:
	; if temp = x93, temp = xD0
	ldi temp, 0xD0
	rjmp NEXT_CURSOR
LIMIT_R2P2:
	; if temp = xD3, temp = xA4
	ldi temp, 0xA4
	rjmp NEXT_CURSOR
LIMIT_R3P2:
	; if temp = xA7, temp = xE4
	ldi temp, 0xE4
	rjmp NEXT_CURSOR
LIMIT_R4P2:
	; if temp = xE7, temp = x90
	ldi temp, 0x90
	rjmp NEXT_CURSOR

; NEXT button instruction
NEXT_CURSOR:
	mov PB, temp
	rcall CHANGE_ROW
	reti


; SHOT button
SHOT_BTN:
	rcall SHOT_TURN	
	reti


START_GAME:
	ldi ZH,high(2*start_text)
	ldi ZL,low(2*start_text)
	ret

INTERFACE_GAME:
	ldi ZH,high(2*interface)
	ldi ZL,low(2*interface)
	ret

INTERFACE_GAME_SHOT:
	ldi ZH,high(2*interface_shot)
	ldi ZL,low(2*interface_shot)
	ret

INIT_LCD_MAIN:
	rcall INIT_LCD

	ser temp
	out DDRA,temp ; Set port A as output
	out DDRB,temp ; Set port B as output

	rcall START_GAME
	rcall LOADBYTE

	rcall DELAY_00

	rcall INTERFACE_GAME
	rcall LOADBYTE

	rcall DELAY_00

	rcall INIT_INPUT_SHIP

	rjmp END_LCD


LOADBYTE:
	lpm ; Load byte from program memory into r0

	mov temp2, r0
	tst temp2	 ; Check if we've reached the end of the message
	breq NEXT_DB ; If so, go to NEXT_DB

	cpi temp2, 1
	breq GOTO_ROW1

	cpi temp2, 2
	breq GOTO_ROW2

	cpi temp2, 3
	breq GOTO_ROW3

	cpi temp2, 4
	breq GOTO_ROW4

	cpi temp2, 5
	breq LOAD_DONE

	mov A, r0 ; Put the character onto Port B
	rcall WRITE_TEXT

	adiw ZL,1 ; Increase Z registers
	rjmp LOADBYTE

LOAD_DONE:
	ret


GOTO_ROW1:
	ldi PB, 0x80
	rcall CHANGE_ROW
	adiw ZL, 1
	rjmp LOADBYTE

GOTO_ROW2:
	ldi PB, 0xC0
	rcall CHANGE_ROW
	adiw ZL, 1
	rjmp LOADBYTE

GOTO_ROW3:
	ldi PB, 0x94
	rcall CHANGE_ROW
	adiw ZL, 1
	rjmp LOADBYTE

GOTO_ROW4:
	ldi PB, 0xD4
	rcall CHANGE_ROW
	adiw ZL, 1
	rjmp LOADBYTE

NEXT_DB:
	adiw ZL, 1
	rjmp LOADBYTE


P1_SHIP_LCD:
	ldi PB, 0xC6	; P1 ship field
	rcall CHANGE_ROW
	mov A, p1_ship
	ori A, 0x30		; Utk nulis ke lcd
	rcall WRITE_TEXT
	rcall DELAY_00
	ret

P2_SHIP_LCD:
	ldi PB, 0xCD	; P2 ship field
	rcall CHANGE_ROW
	mov A, p2_ship
	ori A, 0x30		; Utk nulis ke lcd
	rcall WRITE_TEXT
	rcall DELAY_00
	ret

P1_INPUT_SHIP:
	ldi lamp, 0b01010101 ; LED P1's turn
	out PORTC, lamp

	ldi temp, 0x80
	cbi PORTA,1 ; CLR RS
	mov PB,temp ; MOV DDRAM to 0
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rjmp END_LCD

P2_INPUT_SHIP:
	ldi lamp, 0b10101010 ; LED P2's turn
	out PORTC, lamp	

	ldi temp2, 1
	mov turn_player, temp2
	ldi temp, 0x90
	cbi PORTA,1 ; CLR RS
	mov PB,temp ; MOV DDRAM to 0x90
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rjmp END_LCD

SHOT_TURN:
	mov temp2, stage_game
	cpi temp2, 1
	breq P1_SHOT_P2
	mov temp2, turn_player
	cpi temp2, 0
	breq DEC_SHIP_P1
	cpi temp2, 1
	breq DEC_SHIP_P2

DEC_SHIP_P1:
	//kalo abis kapalnya, pindah turn p2
	mov temp2, p1_ship
	cpi temp2, 0
	breq P2_INPUT_SHIP
	// kalo gak maka taro kapalnya
	//(hrsnya load sram zh zl, simpen/push ke stack)
	mov PB, temp	; P1 input ship
	rcall CHANGE_ROW
	ldi A, 0x4F		; Tulis O ke lcd
	rcall WRITE_TEXT
	rcall DELAY_00

	dec p1_ship
	rcall P1_SHIP_LCD
	ret

DEC_SHIP_P2:
	//kalo abis kapalnya, pindah turn p1
	//(hrsnya load sram zh zl, simpen/push ke stack)
	mov temp2, p2_ship
	cpi temp2, 0
	breq STAGE_SHOT
	// kalo gak maka taro kapalnya
	mov PB, temp	; P2 input ship
	rcall CHANGE_ROW
	ldi A, 0x4F		; Tulis O ke lcd
	rcall WRITE_TEXT
	rcall DELAY_00

	dec p2_ship
	rcall P2_SHIP_LCD
	ret

STAGE_SHOT:
	ldi temp2, 1
	mov stage_game, temp2
	rcall INTERFACE_GAME_SHOT
	rcall LOADBYTE
	rjmp INIT_INPUT_SHIP

P1_SHOT_P2:
	//kalo abis kapal p2, p1 menang
	mov temp2, p2_ship
	cpi temp2, 0
	breq P1_WIN
	// kalo gak maka shot cell p2
	//(hrsnya load sram zh zl, simpen/push ke stack)
	mov PB, temp	; P1 shot ship
	rcall CHANGE_ROW
	ldi A, 0x58		; Tulis X ke lcd
	rcall WRITE_TEXT
	rcall DELAY_00

	dec p2_ship
	rcall P2_SHIP_LCD
	ret

P2_SHOT_P1:
	//kalo abis kapal p1, p2 menang
	mov temp2, p1_ship
	cpi temp2, 0
	breq P2_WIN
	// kalo gak maka shot cell p2
	//(hrsnya load sram zh zl, simpen/push ke stack)
	mov PB, temp	; P2 shot ship
	rcall CHANGE_ROW
	ldi A, 0x58		; Tulis X ke lcd
	rcall WRITE_TEXT
	rcall DELAY_00

	dec p1_ship
	rcall P1_SHIP_LCD
	ret

P1_WIN:
	rcall CLEAR_LCD
    ldi ZH,high(2*p1_win_text)
	ldi ZL,low(2*p1_win_text)
	rcall LOADBYTE
	rcall LED_JALAN
    rjmp EXIT

P2_WIN:
	rcall CLEAR_LCD
	rcall LED_JALAN
    ldi ZH,high(2*p2_win_text)
	ldi ZL,low(2*p2_win_text)
	rcall LOADBYTE
	rcall LED_JALAN
    rjmp EXIT

LED_JALAN:
	ldi lamp, 0b00000001
	out PORTC, lamp
	ldi lamp, 0b00000010
	out PORTC, lamp
	ldi lamp, 0b00000100
	out PORTC, lamp
	ldi lamp, 0b00001000
	out PORTC, lamp
	ldi lamp, 0b00010000
	out PORTC, lamp
	ldi lamp, 0b00100000
	out PORTC, lamp
	ldi lamp, 0b01000000
	out PORTC, lamp
	ldi lamp, 0b10000000
	out PORTC, lamp
	rjmp LED_JALAN

INIT_LCD:
	rcall CLEAR_LCD ; CLEAR LCD
	cbi PORTA,1 ; CLR RS
	ldi PB,0x38 ; MOV DATA,0x38 --> 8bit, 2line, 5x7
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN

	cbi PORTA,1 ; CLR RS
	ldi PB,$0E ; MOV DATA,0x0E --> disp ON, cursor ON, blink OFF
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN

	cbi PORTA,1 ; CLR RS
	ldi PB,$06 ; MOV DATA,0x06 --> increase cursor, display sroll OFF
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	ret

INIT_CURSOR_LCD:
	cbi PORTA,1 ; CLR RS
	ldi PB,$0F ; MOV DATA,0x0F --> disp ON, cursor ON, blink ON
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	ret

INIT_INPUT_SHIP:
	ldi temp, 3
	mov p1_ship, temp
	mov p2_ship, temp
	ldi temp, 0
	mov turn_player, temp
	rcall P1_SHIP_LCD
	rcall P2_SHIP_LCD
	rjmp P1_INPUT_SHIP


CLEAR_LCD:
	cbi PORTA,1 ; CLR RS
	ldi PB,$01 ; MOV DATA,0x01
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_00
	ret

WRITE_TEXT:
	sbi PORTA,1 ; SETB RS
	out PORTB, A
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	//rcall DELAY_00
	ret

CHANGE_ROW:
	cbi PORTA,1 ; CLR RS
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	//rcall DELAY_00
	ret

END_LCD:
	ldi check_int, $01
	ret

DELAY_00:
	; Generated by delay loop calculator
	; at http://www.bretmulvey.com/avrdelay.html
	;
	; Delay 4 000 cycles
	; 500us at 8.0 MHz

	    ldi  r20, 1
	    ldi  r21, 1
	L0: dec  r19
	    brne L0
	    dec  r22
	    brne L0
	ret

;====================================================================
; DATA
;====================================================================

start_text:
.db 0,"     BATTLESHIP     ", 0
.db 2,"        GAME        ", 0
.db 3,"     ==========     ", 0
.db 4,"        ====        ", 5

interface:
.db 1, "    ", FF, "P1      P2", FF, "    ", 0
.db 2, "    ", FF, " X      X ", FF, "    ", 0
.db 3, "    ", FF, "  INPUT   ", FF, "    ", 0
.db 4, "    ", FF, "   SHIPS  ", FF, "    ", 5

interface_shot:
.db 1, "    ", FF, "P1      P2", FF, "    ", 0
.db 2, "    ", FF, " X      X ", FF, "    ", 0
.db 3, "    ", FF, "  SHOT    ", FF, "    ", 0
.db 4, "    ", FF, "   SHIPS  ", FF, "    ", 5

p1_win_text:
	.db 2, "       P1 WON       "
	.db 3, "      GAMEOVER      ", 5

p2_win_text:
	.db 2, "       P1 WON       "
	.db 3, "      GAMEOVER      ", 5
